package rmr;

import jp.tradesc.superkaburobo.sdk.robot.AbstractRobot;
import jp.tradesc.superkaburobo.sdk.trade.EnumCurrentSession;
import jp.tradesc.superkaburobo.sdk.trade.MemoManager;
import jp.tradesc.superkaburobo.sdk.trade.OrderManager;
import jp.tradesc.superkaburobo.sdk.trade.PortfolioManager;
import jp.tradesc.superkaburobo.sdk.trade.TimeManager;
import jp.tradesc.superkaburobo.sdk.trade.TradeAgent;
import jp.tradesc.superkaburobo.sdk.trade.RobotLogManager;
import jp.tradesc.superkaburobo.sdk.trade.InformationManager;
import jp.tradesc.superkaburobo.sdk.trade.KaburoboMath;
import jp.tradesc.superkaburobo.sdk.trade.data.Portfolio;
import jp.tradesc.superkaburobo.sdk.trade.data.Stock;
import jp.tradesc.superkaburobo.sdk.trade.data.StockData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

class Vec implements Serializable {
	private static final long serialVersionUID = 1L;
	private final double[] element;

	Vec(final double[] element) {
		this.element = new double[element.length];
		for (int i = 0; i < element.length; i++) {
			this.element[i] = element[i];
		}
	}

	public static Vec one(final int size) {
		double[] element = new double[size];
		for (int i = 0; i < size; i++) {
			element[i] = 1.0;
		}
		return new Vec(element);
	}

	public static Vec zero(final int size) {
		double[] element = new double[size];
		for (int i = 0; i < size; i++) {
			element[i] = 0.0;
		}
		return new Vec(element);
	}

	public int length() {
		return element.length;
	}

	public double get(int i) {
		return element[i];
	}

	public Vec add(final Vec that) {
		assert (this.element.length == that.element.length);
		double[] newElem = new double[element.length];
		for (int i = 0; i < element.length; i++) {
			newElem[i] = element[i] + that.element[i];
		}
		return new Vec(newElem);
	}

	public double prod(final Vec that) {
		assert (this.element.length == that.element.length);
		double result = 0.0;
		for (int i = 0; i < element.length; i++) {
			result += element[i] * that.element[i];
		}
		return result;
	}

	public Vec prod(final double coef) {
		double[] newElem = new double[element.length];
		for (int i = 0; i < element.length; i++) {
			newElem[i] = element[i] * coef;
		}
		return new Vec(newElem);
	}

	public Vec ewProd(final Vec that) {
		assert (this.element.length == that.element.length);
		double[] newElem = new double[element.length];
		for (int i = 0; i < element.length; i++) {
			newElem[i] = element[i] * that.element[i];
		}
		return new Vec(newElem);
	}
	
	public Vec sub(final Vec that) {
		assert (this.element.length == that.element.length);
		double[] newElem = new double[element.length];
		for (int i = 0; i < element.length; i++) {
			newElem[i] = element[i] - that.element[i];
		}
		return new Vec(newElem);
	}

	public Vec sub(final double x) {
		double[] newElem = new double[element.length];
		for (int i = 0; i < element.length; i++) {
			newElem[i] = element[i] - x;
		}
		return new Vec(newElem);
	}

	public Vec div(final Vec that) {
		assert (this.element.length == that.element.length);
		double[] newElem = new double[element.length];
		for (int i = 0; i < element.length; i++) {
			newElem[i] = element[i] / that.element[i];
		}
		return new Vec(newElem);
	}

	public Vec div(final double x) {
		double[] newElem = new double[element.length];
		for (int i = 0; i < element.length; i++) {
			newElem[i] = element[i] / x;
		}
		return new Vec(newElem);
	}

	public double mean() {
		double result = 0.0;
		for (int i = 0; i < element.length; i++) {
			result += element[i];
		}
		return result / element.length;
	}

	// http://mathworld.wolfram.com/L1-Norm.html
	public double l1norm() {
		double result = 0.0;
		for (int i = 0; i < element.length; i++) {
			result += Math.abs(element[i]);
		}
		return result;
	}

	// http://mathworld.wolfram.com/L2-Norm.html
	public double norm() {
		double result = 0.0;
		for (int i = 0; i < element.length; i++) {
			result += Math.pow(element[i], 2);
		}
		return Math.sqrt(result);
	}

	@Override
	public boolean equals(final Object obj) {
		// 引数が自分自身かどうか
		if (obj == this) {
			return true;
		}
		// 型のチェック、nullチェックも兼ねる
		if (!(obj instanceof Vec)) {
			return false;
		}
		// このキャストの成功は保証されている
		final Vec that = (Vec) obj;

		/*
		 * フィールド1つ1つチェック float,double以外の基本データ型は==で比較
		 * floatはFloat.floatToIntBitsでintに変換して==で比較
		 * doubleはDouble.doubleToLongBitsでlongに変換して==で比較
		 * オブジェクト型は再帰的にequalsメソッドで比較する オブジェクト参照のフィールドとして nullが正当な値となる場合は以下のようにする
		 * (objValue == null ? eql.objValue == null :
		 * eql.objValue.equals(objValue))
		 */
		return Arrays.equals(this.element, that.element);
	}

	@Override
	public int hashCode() {

		/**
		 * 乗数 ※よくわからないけど、奇数の素数がいいみたい 　　　詳しい方教えてくださいm(__)m
		 */
		final int multiplier = 37; //

		/** 任意の値を初期値とする */
		int result = 17;

		result = multiplier * result + Arrays.hashCode(element);

		return result;

	}
	
	@Override
	public String toString() {
		String s = "[";
		for (int i = 0; i < length(); i++) {
			s += get(i);
			if (i != length()-1) {
				s += ",";
			}
		}
		s += "]";
		return s;
	}
}

class RMR {
	private static boolean sameLength(final Vec[] sample) {
		assert sample.length >= 1;
		final int l0 = sample[0].length();
		for (int i = 1; i < sample.length; i++) {
			if (l0 != sample[i].length()) {
				return false;
			}
		}
		return true;
	}

	public static Vec median(final Vec[] sample) {
		double[] medians = new double[sample[0].length()];
		for (int i = 0; i < sample[0].length(); i++) {
			double[] vals = new double[sample.length];
			for (int j = 0; j < sample.length; j++) {
				vals[j] = sample[j].get(i);
			}
			Arrays.sort(vals);
			if (vals.length % 2 == 0) {
				medians[i] = (vals[vals.length / 2] + vals[vals.length / 2 + 1]) / 2.0;
			} else {
				medians[i] = vals[vals.length / 2];
			}
		}
		return new Vec(medians);
	}

	public static Vec T1(final Vec mu, final Vec[] sample) {
		Vec acc1 = Vec.zero(mu.length());
		double acc2 = 0.0;
		for (final Vec x : sample) {
			if (!mu.equals(x)) {
				final double denominator = x.sub(mu).norm();
				acc1 = acc1.add(x.div(denominator));
				acc2 += 1.0 / denominator;
			}
		}
		return acc1.div(acc2);
	}

	public static double f(final Vec mu, final Vec[] sample) {
		for (final Vec x : sample) {
			if (mu.equals(x)) {
				return 1.0;
			}
		}
		return 0.0;
	}

	public static Vec R(final Vec mu, final Vec[] sample) {
		Vec acc = Vec.zero(mu.length());
		for (final Vec x : sample) {
			if (!mu.equals(x)) {
				final Vec diff = x.sub(mu);
				acc = acc.add(diff.div(diff.norm()));
			}
		}
		return acc;
	}

	public static double g(final Vec mu, final Vec[] sample) {
		return Math.min(1.0, f(mu, sample) / R(mu, sample).norm());
	}

	public static Vec T(final Vec mu, final Vec[] sample) {
		assert sameLength(sample);
		assert mu.length() == sample[0].length();
		final double coef = g(mu, sample);
		return T1(mu, sample).prod(1.0 - coef).add(mu.prod(coef));
	}

	public static Vec l1median(final Vec[] sample, final int iterMax,
			final double toleLevel) {
		Vec mu = median(sample);
		for (int i = 1; i < iterMax; i++) {
			final Vec nextMu = T(mu, sample);
			if (mu.sub(nextMu).l1norm() <= toleLevel * nextMu.l1norm()) {
				break;
			}
			mu = nextMu;
		}
		return mu;
	}

	public static Vec normalize(final Vec portfolio) {
		double[] xs = new double[portfolio.length()];
		for (int i = 0; i < portfolio.length(); i++) {
			xs[i] = portfolio.get(i);
		}
		Arrays.sort(xs);
		double[] sortedXs = new double[xs.length];
		for (int i = 0; i < xs.length; i++) {
			sortedXs[i] = xs[xs.length - i - 1];
		}

		int p = -1;
		for (int i = 0; i < sortedXs.length; i++) {
			double acc = 0.0;
			for (int r = 0; r <= i; r++) {
				acc += sortedXs[r];
			}
			acc -= 1;
			acc /= (i+1);
			if (sortedXs[i] - acc > 0.0) {
				p = i;
			}
		}
		
/*		for (int i = 0; i < sortedXs.length; i++) {
			System.out.print(sortedXs[i]);
			System.out.print(",");
		}
	*/	
		if (p == -1) {
//			System.exit(-1);
		}

		double acc = 0.0;
		for (int r = 0; r <= p; r++) {
			acc += sortedXs[r];
		}
		acc -= 1;
		acc /= (p+1);

		double[] newXs = new double[portfolio.length()];
		for (int i = 0; i < portfolio.length(); i++) {
			newXs[i] = Math.max(portfolio.get(i) - acc, 0.0);
		}

		return new Vec(newXs);
	}

	public static Vec step(final double threshold, final Vec nextPrice,
			final Vec curPort) {
		assert nextPrice.length() == curPort.length();
		final double numerator = nextPrice.prod(curPort) - threshold;
		final Vec diff = nextPrice.sub(Vec.one(curPort.length()).prod(
				nextPrice.mean()));
		final double denominator = Math.pow(diff.norm(), 2.0);
		final double alpha = Math.min(0, numerator / denominator);
		final Vec nextPort = curPort.sub(diff.prod(alpha));
		final Vec result = normalize(nextPort);
		return result;
	}
	
	public static Vec[] slice(final Vec[] xs, final int start, final int end) {
		final int period = end - start;
		assert period >= 0;
		final Vec[] result = new Vec[period];
		for (int i = 0; i < period; i++) {
			result[i] = xs[i + start];
		}
		return result;
	}

	public static List<Double> psWithRmr(Vec[] prvs, double threshold, final int iterMax,
			final double toleLevel, final int period) {
		double cr = 0.0;
		Vec[] ps = new Vec[prvs.length + 1];
		final Vec b0 = Vec.one(prvs[0].length()).div(prvs[0].length());
		Vec portfolio = b0;
		final List<Double> rs = new ArrayList<Double>();
		ps[0] = Vec.one(prvs[0].length());
		for (int i = 0; i < prvs.length; i++) {
			ps[i + 1] = prvs[i].ewProd(ps[i]);
		}
		for (int i = period-1; i < prvs.length; i++) {
//			System.out.println(cr + ",");
			final double r = (portfolio.prod(prvs[i]) - 1.0 - 0.001);//0.00787);
			cr += r;
			rs.add(r);
			final Vec next = l1median(slice(ps, i+1-period, i+1), iterMax, toleLevel);
			portfolio = step(threshold, next.div(ps[i+1]), portfolio);
		}
		return rs;
	}
}

class RMRTest {
	public static Vec randomVec(final Random random, final int dim,
			final double min, final double max) {
		final double[] elems = new double[dim];
		for (int i = 0; i < dim; i++) {
			elems[i] = random.nextDouble() * (max - min) + min;
		}
		return new Vec(elems);
	}

	public static void testProjectionOntoSimplex() {
		final Random random = new Random(0);
		final int size = 1000;
		final int dim = 10;
		final Vec[] arbVecs = new Vec[size];
		for (int i = 0; i < size; i++) {
			arbVecs[i] = randomVec(random, dim, 0, 1);
		}

		final Vec x = randomVec(random, dim, -10, 10);
		final Vec y = RMR.normalize(x);
		final double xyDistance = x.sub(y).norm();

		for (int i = 0; i < size; i++) {
			if (arbVecs[i].sub(y).norm() < xyDistance) {
				assert false;
				return;
			}
		}

		assert true;
	}
}

class RMRState implements Serializable {
	private static final long serialVersionUID = 1L;
	public List<Vec> prices;
	public Vec portfolio;
	public Vec first;
	public Map<Integer,Integer> code2idx;

	RMRState(List<Vec> prices, Vec portfolio, Vec first) {
		this.prices = prices;
		this.portfolio = portfolio;
		this.first = first;
	}

	public void addPrice(Vec price) {
		prices.remove(0);
		prices.add(price);
	}
}

public class RMRRobot extends AbstractRobot {

	public static Vec makePriceVec(InformationManager im) {
		List<Stock> stocks = removeDefictStock(im.getStockList());
		List<Double> newStocks = new ArrayList<Double>();
		for (int i = 0; i < stocks.size(); i++) {
//			if (validStockCode.contains(stocks.get(i).getStockCode())) {
				StockData sd = im.getStockSession(stocks.get(i));
				newStocks.add((double) (sd.getClosingPrice() * stocks.get(i).getUnit()));
//			}
		}
		double[] elems = new double[newStocks.size()];
		for (int i = 0; i < elems.length; i++) {
			elems[i] = newStocks.get(i);
		}
		return new Vec(elems);
	}

	public static List<Stock> removeDefictStock(final List<Stock> stocks) {
		final List<Stock> newStocks = new ArrayList<Stock>();
		for (final Stock stock : stocks) {
			if (!defictStockCode.contains(stock.getStockCode())) {
				newStocks.add(stock);
			}
		}
		return newStocks;
	}

	private static final Set<Integer> defictStockCode;
	private static final Set<Integer> validStockCode;
	static {
		final int[] codes = { /* data50 */4568, 6460, 5802,/* data300 */1605 };
		final int[] validCodes = {1332,1721,1801,1802,1803,1808,1812,1860,1878,1925,1928,1942,1951,1963,2002,2261,2267,2282,2331,2433,2501,2502,2503,2579,2593,2651,2670,2730,2768,2779,2784,2801,2802,2897,2914,3101,3105,3332,3337,3382,3401,3402,3404,3405,3407,3591,3861,3893,4004,4005,4043,4062,4063,4091,4118,4182,4183,4185,4188,4204,4208,4217,4307,4321,4324,4452,4502,4503,4507,4519,4523,4535,4543,4631,4661,4676,4680,4681,4704,4716,4723,4732,4756,4768,4901,4902,4911,5001,5016,5108,5110,5201,5214,5233,5333,5334,5401,5405,5406,5407,5411,5471,5486,5631,5711,5713,5801,5803,5901,5929,5938,5943,5947,5991,6113,6146,6201,6273,6301,6302,6305,6326,6361,6366,6367,6370,6417,6448,6471,6472,6473,6479,6481,6501,6502,6503,6504,6581,6586,6701,6702,6723,6724,6752,6753,6758,6762,6764,6773,6806,6841,6857,6861,6902,6923,6952,6954,6967,6971,6988,6991,7011,7012,7013,7201,7202,7203,7211,7259,7261,7267,7269,7272,7453,7459,7532,7731,7733,7741,7751,7752,7832,7862,7911,7912,7915,7936,7951,7966,7984,8001,8002,8012,8015,8016,8028,8031,8035,8036,8053,8056,8058,8060,8078,8113,8129,8136,8184,8218,8227,8233,8234,8238,8242,8252,8253,8267,8268,8270,8273,8281,8282,8303,8306,8308,8309,8316,8326,8328,8331,8332,8333,8355,8377,8403,8404,8411,8473,8515,8564,8572,8574,8583,8591,8601,8604,8606,8615,8752,8766,8801,8802,8804,8815,8830,8840,8868,8870,8874,9005,9020,9021,9022,9041,9042,9062,9064,9101,9104,9202,9301,9364,9401,9404,9432,9433,9437,9501,9503,9531,9532,9602,9613,9684,9735,9737,9744,9747,9766,9831,9843,9875,9962,9984,9987,9989 };
		final Set<Integer> s = new HashSet<Integer>();
		for (int i = 0; i < codes.length; i++) {
			s.add(codes[i]);
		}
		defictStockCode = s;
		final Set<Integer> s2 = new HashSet<Integer>();
		for (int i = 0; i < validCodes.length; i++) {
			s2.add(validCodes[i]);
		}
		validStockCode = s2;
	}

	// オーダーメソッド
	@Override
	public void order(TradeAgent tradeagent) {
		// 処理を記述
		final TimeManager tm = TimeManager.getInstance();
		if (tm.getCurrentSession() == EnumCurrentSession.EARLY_SESSION) {
			final InformationManager im = InformationManager.getInstance();
			final Vec price = makePriceVec(im);
			if (price.length() == 0) {
				return;
			}

			final MemoManager mm = MemoManager.getInstance();
			final RMRState state = (RMRState) mm.getObjectMemo();

			final Long totalAsset = tradeagent.getTotalAssetValue();
			final Long priceForOne = (long)(totalAsset / (price.length() * 1.2));

			if (state == null) {
				// TODO:normalizeと初期値処理
				List<Vec> prices = new ArrayList<Vec>();
				Vec first = price;
				prices.add(price.div(first));
				final RMRState newState = new RMRState(prices, Vec.one(
						price.length()).div(price.length()), first);
				mm.setObjectMemo(newState);

				final List<Stock> stocks = removeDefictStock(im.getStockList());
				assert stocks.size() == price.length();
				final OrderManager om = OrderManager.getInstance();
				for (int i = 0; i < stocks.size(); i++) {
					final Stock stock = stocks.get(i);
					final Integer variationAmount = (int) (priceForOne / im
							.getStockSession(stock).getClosingPrice());
					if (variationAmount > stock.getUnit()) {
						//om.orderActualClosingMarket(stock, Math.max(variationAmount,stock.getUnit()));
					}
				}

				return;
			} else if (state.prices.size() < 5) {
				state.prices.add(price);
				mm.setObjectMemo(state);
				return;
			}

			state.addPrice(price.div(state.first));

			final Vec[] vecs = new Vec[state.prices.size()];
			state.prices.toArray(vecs);

			final Vec curPort = state.portfolio;
			final Vec nextPrice = RMR.l1median(vecs, 200, 0.001);
			final Vec nextPort = RMR.step(10e6, nextPrice.div(price.div(state.first)), curPort);
			state.portfolio = nextPort;
			mm.setObjectMemo(state);
			
			final List<Stock> stocks = removeDefictStock(im.getStockList());
			assert stocks.size() == price.length();
			final OrderManager om = OrderManager.getInstance();
			final PortfolioManager pm = PortfolioManager.getInstance();
			
			if (curPort.equals(nextPort)) {
				for (int i = 0; i < stocks.size(); i++) {
					final double nextWeight = nextPort.get(i);
					final Stock stock = stocks.get(i);
					if (nextWeight >= 0.5) {
						final int qty = 
						  (int)(tradeagent.getTotalAssetValue() / im.getStockDaily(stock).getClosingPrice());
						om.orderActualNowMarket(stock, qty);
						break;
					}
				}
				return;
			}
						
			for (final Portfolio p : pm.getPortfolio()) {
				p.orderReverseNowMarketAll();
			}
						
			for (int i = 0; i < stocks.size(); i++) {
				final double nextWeight = nextPort.get(i);
				final Stock stock = stocks.get(i);
				if (nextWeight >= 0.5) {
					final int qty = 
					  (int)(tradeagent.getTotalAssetValue() / im.getStockDaily(stock).getClosingPrice());
					om.orderActualNowMarket(stock, qty);
					break;
				}
			}
			

		}
	}
	

	// スクリーニングメソッド
	@Override
	public void screening(TradeAgent tradeagent) {
		// ログマネージャのオブジェクト取得
		// RobotLogManager logmanager = tradeagent.getRobotLogManager();

	}
}

