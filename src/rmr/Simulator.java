package rmr;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

public class Simulator {

	public static double sharpeRatio(final List<Double> xs) {
		double mean = 0.0;
		for (int i = 0; i < xs.size(); i++) {
			mean += xs.get(i);
		}
		mean /= xs.size();
		double sd = 0.0;
		for (int i = 0; i < xs.size(); i++) {
			sd += Math.pow(xs.get(i) - mean, 2);
		}
		sd /= xs.size();
		sd = Math.sqrt(sd);
		return mean / sd;
	}
	
	public static List<Vec> sampling(Random random, int size, List<Vec> prvs) {
		int[] idxs = new int[size];
		for (int i = 0; i < size; i++) {
			idxs[i] = random.nextInt(prvs.get(0).length());
		}
		
		List<Vec> result = new ArrayList<Vec>();
		for (int i = 0; i < prvs.size(); i++) {
			double[] elems = new double[size];
			for (int j = 0; j < size; j++) {
				elems[j] = prvs.get(i).get(idxs[j]);
			}
			result.add(new Vec(elems));
		}
		return result;
	}
	
	public static List<Double> combineResults(List<List<Double>> results) {
		List<Double> result = new ArrayList<Double>();
		for (int i = 0; i < results.get(0).size(); i++) {
			double mean = 0.0;
			for (int j = 0; j < results.size(); j++) {
				mean += results.get(j).get(i);
			}
			mean /= results.size();
			result.add(mean);
		}
		return result;
	}
	
	public static void simulationWithMyData() {
		List<Vec> prvs = new ArrayList<Vec>();
        try {
            //ファイルを読み込む
            FileReader fr = new FileReader("C:\\Users\\Owner\\prvs_high.txt");
            BufferedReader br = new BufferedReader(fr);

            String line;
            while ((line = br.readLine()) != null) {
                String[] tokens = line.split(",");

                //分割した文字を画面出力する
                double[] elems = new double[tokens.length];
                for (int i = 0; i < tokens.length; i++) {
                    elems[i] = Double.parseDouble(tokens[i]);
                }
                prvs.add(new Vec(elems));
//                System.out.println("**********");
            }

            //終了処理
            br.close();

        } catch (IOException ex) {
            //例外発生時処理
            ex.printStackTrace();
        }
        double threshold = 3;//100000;
        int iterMax = 200;
        double toleLevel = 0.001;
        int period = 5;
        {
        Vec[] xs = new Vec[prvs.size()];
        System.out.println(prvs.get(0).length());
        prvs.toArray(xs);
		List<Double> result = RMR.psWithRmr(xs, threshold, iterMax, toleLevel, period);
		System.out.println(sharpeRatio(result));
		double cr = 0.0;
		for (int i = 0; i < result.size(); i++) {
			cr += result.get(i);
			System.out.println(cr + ",");
		}
        }

        Random random = new Random();
        List<List<Double>> results = new ArrayList<List<Double>>();
        for (int i = 0; i < 1; i++) {
	        List<Vec> sample = sampling(random, 5, prvs);
	        Vec[] xs = new Vec[sample.size()];
	        sample.toArray(xs);
			List<Double> result = RMR.psWithRmr(xs, threshold, iterMax, toleLevel, period);
			results.add(result);
			System.out.println(i);
        }
        List<Double> finalResult = combineResults(results);
        System.out.println(finalResult.size());
		System.out.println(sharpeRatio(finalResult));
		double cr = 0.0;
		for (int i = 0; i < finalResult.size(); i++) {
			cr += finalResult.get(i);
//			System.out.println(cr + ",");
		}
//		System.out.println(cr);
	}
}
